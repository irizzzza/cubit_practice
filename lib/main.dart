import 'package:cubit_practice/cubit/colors_cubit.dart';
import 'package:cubit_practice/pages/home_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_cubit/flutter_cubit.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(

        primarySwatch: Colors.blue,

        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: CubitProvider(
          create: (context) => ColorsCubit(),
          child: HomePage()),
    );
  }
}