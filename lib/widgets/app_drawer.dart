import 'package:cubit_practice/cubit/colors_cubit.dart';
import 'package:cubit_practice/res/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_cubit/flutter_cubit.dart';

class AppDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView.builder(
        itemCount: appColors.length,
        itemBuilder: (BuildContext ctx, int index) {
          return RaisedButton(
            onPressed: () {
              context.cubit<ColorsCubit>().setCol(index);
              //  state = index;
              Navigator.of(context).pop();
            },
            color: appColors[index],
          );
        },
      ),
    );
  }
}
