import 'dart:ui';
import 'package:cubit/cubit.dart';
import 'package:cubit_practice/model/bg_color.dart';
import 'package:cubit_practice/res/colors.dart';
import 'package:flutter/material.dart';

class ColorsCubit extends Cubit<int>{
  ColorsCubit() : super(
   0
  );


void setCol(int index){
  emit(index);
}

}