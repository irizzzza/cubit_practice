import 'package:cubit_practice/cubit/colors_cubit.dart';
import 'package:cubit_practice/model/bg_color.dart';
import 'package:cubit_practice/res/colors.dart';
import 'package:cubit_practice/widgets/app_drawer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_cubit/flutter_cubit.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return CubitBuilder<ColorsCubit, int>(
    builder: (BuildContext context, int state) {
      return Scaffold(
        backgroundColor: appColors[state],
        appBar: AppBar(),
        drawer: AppDrawer(),
      );
    });
  }
}
