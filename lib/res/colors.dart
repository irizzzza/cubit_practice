import 'package:flutter/material.dart';

List<Color> appColors = [
  Colors.blueAccent,
  Colors.blue,
  Colors.cyan,
  Colors.orange,
  Colors.deepOrangeAccent,
  Colors.deepOrange,
  Colors.red
];